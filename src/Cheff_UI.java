import java.awt.EventQueue;
//import java.awt.event.KeyEvent;
//import java.sql.*
import java.sql.*;


import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

//import com.mysql.cj.xdevapi.Statement;
//import com.sun.jdi.connect.spi.Connection;

import java.awt.Font;
//import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.ImageIcon;
import java.awt.Color;
import javax.swing.UIManager;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
//import javax.swing.JFormattedTextField;
import java.awt.event.KeyAdapter;
import java.awt.SystemColor;public class Cheff_UI {

	JFrame frame;
	private JTable table;
	DefaultTableModel model;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Cheff_UI window = new Cheff_UI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Cheff_UI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(46, 139, 87));
		frame.setBackground(new Color(30, 144, 255));
		frame.setBounds(100, 100, 800, 500);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 10, 349, 443);
		frame.getContentPane().add(scrollPane);
		
		table = new JTable();
		table.setBackground(new Color(105, 105, 105));
		scrollPane.setViewportView(table);
		model = new DefaultTableModel();
		table.setModel(model);
		scrollPane.setVisible(true);
		
		
		
	    Object [] columns = {"Food ID", "Food Quantity", "Order_ID"};
	    model.setColumnIdentifiers(columns);
	    
		table.getColumnModel().getColumn(0).setPreferredWidth(60);
		table.getColumnModel().getColumn(0).setMinWidth(60);
		table.getColumnModel().getColumn(0).setMaxWidth(64);
		table.getColumnModel().getColumn(1).setPreferredWidth(126);
		table.getColumnModel().getColumn(1).setMinWidth(110);
		table.getColumnModel().getColumn(1).setMaxWidth(126);
		
		table.setRowHeight(22);
		
	//----------------------------------loading data from database to table----------------------------------------
		
		Object[] row = new Object[3];
			try { 
			   Class.forName("com.mysql.jdbc.Driver");  
			   java.sql.Connection con = DriverManager.getConnection("jdbc:mysql://localhost/food_menu", "root", "");   
			   java.sql.Statement statement = con.createStatement();
			   String sql = "SELECT * FROM `ordes` WHERE 1";      
			   ResultSet rs= statement.executeQuery(sql);

			   while (rs.next()) {
				  row[0] = rs.getString("Food ID");
		          row[1] = rs.getString("Food Quantity");
		          row[2] = rs.getString("Order_ID");
				  model.addRow(row);
			   }
			   statement.close();  
			   con.close();  
			} catch (SQLException | ClassNotFoundException ex) {  
				 JOptionPane.showMessageDialog(null, ex);  
			}
		
//		table = new JTable();
//		table.setFont(new Font("Tahoma", Font.BOLD, 18));
//		table.setModel(new DefaultTableModel(
//			new Object[][] {
//				{"Customer ID", "Order ID", "Item Name", "Quantity"},
//				{null, null, null, null},
//				{null, null, null, null},
//				{null, null, null, null},
//				{null, null, null, null},
//				{null, null, null, null},
//			},
//			new String[] {
//				"Customer ID", "Order ID", "Item Name", "Quantity"
//			}
//		));
//		table.getColumnModel().getColumn(0).setPreferredWidth(100);
//		table.getColumnModel().getColumn(2).setPreferredWidth(85);
//		table.setBounds(10, 70, 463, 300);
//		frame.getContentPane().add(table);
//		table.setRowHeight(50);
		
		final JButton btnNewButton_1 = new JButton("Proceed");
		btnNewButton_1.setBounds(369, 31, 85, 22);
		btnNewButton_1.addKeyListener(new KeyAdapter() {

		});
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(btnNewButton_1, "order Completed!");
				btnNewButton_1.setBackground(Color.GREEN);
			}
		});
		btnNewButton_1.setBackground(UIManager.getColor("Button.background"));
		btnNewButton_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		frame.getContentPane().add(btnNewButton_1);
		btnNewButton_1.addKeyListener(new KeyAdapter() {

		});
		
		final JButton btnNewButton_1_1 = new JButton("Proceed");
		btnNewButton_1.addKeyListener(new KeyAdapter() {

		});
		btnNewButton_1_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(btnNewButton_1_1, "order Completed!");
				btnNewButton_1_1.setBackground(Color.GREEN);
			}});
		btnNewButton_1_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnNewButton_1_1.setBackground(UIManager.getColor("Button.background"));
		btnNewButton_1_1.setBounds(369, 51, 85, 22);
		frame.getContentPane().add(btnNewButton_1_1);

		final JButton btnNewButton_1_2 = new JButton("Proceed");
		btnNewButton_1_2.addKeyListener(new KeyAdapter() {

		});
		btnNewButton_1_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(btnNewButton_1_2, "order Completed!");
				btnNewButton_1_2.setBackground(Color.GREEN);
			}});
		btnNewButton_1_2.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnNewButton_1_2.setBackground(UIManager.getColor("Button.background"));
		btnNewButton_1_2.setBounds(369, 91, 85, 17);
		frame.getContentPane().add(btnNewButton_1_2);
		
		final JButton btnNewButton_1_3 = new JButton("Proceed");
		btnNewButton_1_3.addKeyListener(new KeyAdapter() {

		});
		btnNewButton_1_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(btnNewButton_1_3, "order Completed!");
				btnNewButton_1_3.setBackground(Color.GREEN);
			}});
		btnNewButton_1_3.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnNewButton_1_3.setBackground(SystemColor.menu);
		btnNewButton_1_3.setBounds(369, 71, 85, 17);
		frame.getContentPane().add(btnNewButton_1_3);
		
		final JButton btnNewButton_1_4 = new JButton("Proceed");
		btnNewButton_1_4.addKeyListener(new KeyAdapter() {

		});
		btnNewButton_1_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(btnNewButton_1_4, "order Completed!");
				btnNewButton_1_4.setBackground(Color.GREEN);
			}});
		btnNewButton_1_4.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnNewButton_1_4.setBackground(UIManager.getColor("Button.background"));
		btnNewButton_1_4.setBounds(369, 11, 85, 17);
		frame.getContentPane().add(btnNewButton_1_4);
		
		final JButton btnNewButton_1_5 = new JButton("Proceed");
		btnNewButton_1_5.addKeyListener(new KeyAdapter() {

		});
		btnNewButton_1_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(btnNewButton_1_5, "order Completed!");
				btnNewButton_1_5.setBackground(Color.GREEN);
			}});
		btnNewButton_1_5.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnNewButton_1_5.setBackground(UIManager.getColor("Button.background"));
		btnNewButton_1_5.setBounds(369, 165, 85, 17);
		frame.getContentPane().add(btnNewButton_1_5);
		
		final JButton btnNewButton_1_6 = new JButton("Proceed");
		btnNewButton_1_6.addKeyListener(new KeyAdapter() {

		});
		btnNewButton_1_6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(btnNewButton_1_6, "order Completed!");
				btnNewButton_1_6.setBackground(Color.GREEN);
			}});
		btnNewButton_1_6.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnNewButton_1_6.setBackground(UIManager.getColor("Button.background"));
		btnNewButton_1_6.setBounds(369, 192, 85, 17);
		frame.getContentPane().add(btnNewButton_1_6);
		
		final JButton btnNewButton_1_7 = new JButton("Proceed");
		btnNewButton_1_7.addKeyListener(new KeyAdapter() {

		});
		btnNewButton_1_7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(btnNewButton_1_7, "order Completed!");
				btnNewButton_1_7.setBackground(Color.GREEN);
			}});
		btnNewButton_1_7.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnNewButton_1_7.setBackground(UIManager.getColor("Button.background"));
		btnNewButton_1_7.setBounds(369, 219, 85, 17);
		frame.getContentPane().add(btnNewButton_1_7);
		
		final JButton btnNewButton_1_8 = new JButton("Proceed");
		btnNewButton_1_8.addKeyListener(new KeyAdapter() {

		});
		btnNewButton_1_8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(btnNewButton_1_8, "order Completed!");
				btnNewButton_1_8.setBackground(Color.GREEN);
			}});
		btnNewButton_1_8.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnNewButton_1_8.setBackground(UIManager.getColor("Button.background"));
		btnNewButton_1_8.setBounds(369, 246, 85, 17);
		frame.getContentPane().add(btnNewButton_1_8);
		
		final JButton btnNewButton_1_9 = new JButton("Proceed");
		btnNewButton_1_9.addKeyListener(new KeyAdapter() {

		});
		btnNewButton_1_9.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(btnNewButton_1_9, "order Completed!");
				btnNewButton_1_9.setBackground(Color.GREEN);
			}});
		btnNewButton_1_9.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnNewButton_1_9.setBackground(UIManager.getColor("Button.background"));
		btnNewButton_1_9.setBounds(369, 269, 85, 17);
		frame.getContentPane().add(btnNewButton_1_9);
		
		final JButton btnNewButton_1_10 = new JButton("Proceed");
		btnNewButton_1_10.addKeyListener(new KeyAdapter() {

		});
		btnNewButton_1_10.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(btnNewButton_1_10, "order Completed!");
				btnNewButton_1_10.setBackground(Color.GREEN);
			}});
		btnNewButton_1_10.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnNewButton_1_10.setBackground(UIManager.getColor("Button.background"));
		btnNewButton_1_10.setBounds(369, 296, 85, 17);
		frame.getContentPane().add(btnNewButton_1_10);
		
		final JButton btnNewButton_1_11 = new JButton("Proceed");
		btnNewButton_1_11.addKeyListener(new KeyAdapter() {

		});
		btnNewButton_1_11.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(btnNewButton_1_11, "order Completed!");
				btnNewButton_1_11.setBackground(Color.GREEN);
			}});
		btnNewButton_1_11.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnNewButton_1_11.setBackground(UIManager.getColor("Button.background"));
		btnNewButton_1_11.setBounds(369, 323, 85, 17);
		frame.getContentPane().add(btnNewButton_1_11);
		
		final JButton btnNewButton_1_12 = new JButton("Proceed");
		btnNewButton_1_12.addKeyListener(new KeyAdapter() {

		});
		btnNewButton_1_12.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(btnNewButton_1_12, "order Completed!");
				btnNewButton_1_12.setBackground(Color.GREEN);
			}});
		btnNewButton_1_12.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnNewButton_1_12.setBackground(UIManager.getColor("Button.background"));
		btnNewButton_1_12.setBounds(369, 342, 85, 17);
		frame.getContentPane().add(btnNewButton_1_12);
		
		final JButton btnNewButton_1_13 = new JButton("Proceed");
		btnNewButton_1_13.addKeyListener(new KeyAdapter() {

		});
		btnNewButton_1_13.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(btnNewButton_1_13, "order Completed!");
				btnNewButton_1_13.setBackground(Color.GREEN);
			}});
		btnNewButton_1_13.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnNewButton_1_13.setBackground(UIManager.getColor("Button.background"));
		btnNewButton_1_13.setBounds(369, 363, 85, 17);
		frame.getContentPane().add(btnNewButton_1_13);
		
		final JButton btnNewButton_1_14 = new JButton("Proceed");
		btnNewButton_1_14.addKeyListener(new KeyAdapter() {

		});
		btnNewButton_1_14.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(btnNewButton_1_14, "order Completed!");
				btnNewButton_1_14.setBackground(Color.GREEN);
			}});
		btnNewButton_1_14.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnNewButton_1_14.setBackground(UIManager.getColor("Button.background"));
		btnNewButton_1_14.setBounds(369, 383, 85, 17);
		frame.getContentPane().add(btnNewButton_1_14);
		
		JButton btnNewButton = new JButton("Main Menu");
		btnNewButton.setFont(new Font("Tahoma", Font.BOLD, 20));
		btnNewButton.setBounds(546, 170, 167, 58);
		frame.getContentPane().add(btnNewButton);
		btnNewButton.addActionListener(new ActionListener() {
			
			
			public void actionPerformed(ActionEvent e) {
				frame.dispose();
				Homepage main = new Homepage();
				main.frame.setVisible(true);
				// TODO Auto-generated method stub
				
			}
		});
		
		JButton btnClearAll = new JButton("Clear All");
		btnClearAll.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				while (model.getRowCount()>0)
		          {
		             model.removeRow(0);
		          }
				btnNewButton_1.setBackground(null);
				btnNewButton_1_1.setBackground(null);
				btnNewButton_1_2.setBackground(null);
				btnNewButton_1_3.setBackground(null);
				btnNewButton_1_4.setBackground(null);
				btnNewButton_1_5.setBackground(null);
				btnNewButton_1_6.setBackground(null);
				btnNewButton_1_7.setBackground(null);
				btnNewButton_1_8.setBackground(null);
				btnNewButton_1_9.setBackground(null);
				btnNewButton_1_10.setBackground(null);
				btnNewButton_1_11.setBackground(null);
				btnNewButton_1_12.setBackground(null);
				btnNewButton_1_13.setBackground(null);
				btnNewButton_1_14.setBackground(null);
                
if (e.getSource()== btnClearAll) {
					
				    try {  
				        Class.forName("com.mysql.jdbc.Driver");  
				        // establish connection  
				        java.sql.Connection con = DriverManager.getConnection("jdbc:mysql://localhost/food_menu", "root", "");   
				        java.sql.Statement statement2 = con.createStatement();
				        
				        
				       statement2.executeUpdate("DELETE  FROM ordes"); 
				       JOptionPane.showMessageDialog(null, "ALL ORDERS CLEARED  ! ");
				        
				       statement2.close(); 
				       con.close();  
				     
				    } catch (SQLException | ClassNotFoundException e1) {  
				        JOptionPane.showMessageDialog(null, e1);  
				    }  
				}

			}
			
			
			
		});
		btnClearAll.setFont(new Font("Tahoma", Font.BOLD, 20));
		btnClearAll.setBounds(546, 273, 167, 58);
		frame.getContentPane().add(btnClearAll);
		
		JLabel lblNewLabel_1 = new JLabel("New label");
		lblNewLabel_1.setBackground(new Color(30, 144, 255));
		lblNewLabel_1.setIcon(new ImageIcon("C:\\Users\\User\\Desktop\\ch.jpg"));
		lblNewLabel_1.setBounds(369, 10, 407, 443);
		frame.getContentPane().add(lblNewLabel_1);
		
//		JScrollPane scrollPane_1 = new JScrollPane();
//		scrollPane_1.setBounds(728, 53, 492, 595);
//		frame.getContentPane().add(scrollPane_1);
//		
//		table1 = new JTable();
//		scrollPane_1.setViewportView(table1);
//		table1.setModel(model1);
//		
//		table1.getColumnModel().getColumn(0).setPreferredWidth(60);
//		table1.getColumnModel().getColumn(0).setMinWidth(60);
//		table1.getColumnModel().getColumn(0).setMaxWidth(64);
//		table1.getColumnModel().getColumn(1).setPreferredWidth(126);
//		table1.getColumnModel().getColumn(1).setMinWidth(110);
//		table1.getColumnModel().getColumn(1).setMaxWidth(126);
//		
//		table1.setRowHeight(22);
//		model1 = new DefaultTableModel();
//		scrollPane_1.setVisible(true);
//		
//		
//		
//	    Object [] clumns = {"food_id", "food_name","food_price", "food_descript"};
//	    model1.setColumnIdentifiers(clumns);
//		
//	//----------------------------------loading data from database to table----------------------------------------
//		
//		Object[] rw = new Object[4];
//			try { 
//			   Class.forName("com.mysql.cj.jdbc.Driver");  
//			   java.sql.Connection con = DriverManager.getConnection("jdbc:mysql://localhost/food_menu", "root", "");   
//			   java.sql.Statement statement = con.createStatement();
//			   String sql = "SELECT * FROM `food_table` WHERE 1";      
//			   ResultSet rs= statement.executeQuery(sql);
//
//			   while (rs.next()) {
//				  rw[0] = rs.getString("food_id");
//		          rw[1] = rs.getString("food_name");
//		          rw[2] = rs.getString("food_price");
//		          rw[3] = rs.getString("food_descript");
//				  model1.addRow(rw);
//			   }
//			   statement.close();  
//			   con.close();  
//			} catch (SQLException | ClassNotFoundException ex) {  
//				 JOptionPane.showMessageDialog(null, ex);  
//			}
}
}