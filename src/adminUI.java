import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import com.mysql.jdbc.Connection;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTable;
import java.awt.Font;
import javax.swing.JTextArea;
import java.awt.Color;
import javax.swing.JScrollPane;
import java.awt.SystemColor;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.DriverManager;
import java.sql.SQLException;
import javax.swing.JRadioButton;
import javax.swing.ImageIcon;

public class AdminUI {

	JFrame frame;
	private JTextField food_id;
	private JTextField food_name;
	private JTextField food_price;
	private JTable table;
	DefaultTableModel model;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AdminUI window = new AdminUI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public AdminUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(SystemColor.activeCaption);
		frame.setBounds(100, 100, 873, 519);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Food ID");
		lblNewLabel.setForeground(Color.WHITE);
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblNewLabel.setBounds(56, 117, 81, 24);
		frame.getContentPane().add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Food Price");
		lblNewLabel_1.setForeground(Color.WHITE);
		lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblNewLabel_1.setBounds(56, 220, 98, 24);
		frame.getContentPane().add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("Food Name");
		lblNewLabel_2.setForeground(Color.WHITE);
		lblNewLabel_2.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblNewLabel_2.setBounds(56, 166, 98, 24);
		frame.getContentPane().add(lblNewLabel_2);
		
		food_id = new JTextField();
		food_id.setBounds(205, 120, 114, 24);
		frame.getContentPane().add(food_id);
		food_id.setColumns(10);
		
		food_name = new JTextField();
		food_name.setBounds(205, 169, 114, 24);
		frame.getContentPane().add(food_name);
		food_name.setColumns(10);
		
		food_price = new JTextField();
		food_price.setBounds(205, 223, 114, 24);
		frame.getContentPane().add(food_price);
		food_price.setColumns(10);
		
		JLabel lblNewLabel_3 = new JLabel("Food Discription");
		lblNewLabel_3.setForeground(Color.WHITE);
		lblNewLabel_3.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblNewLabel_3.setBounds(56, 266, 130, 24);
		frame.getContentPane().add(lblNewLabel_3);
		
		JTextArea food_descript = new JTextArea();
		food_descript.setBounds(205, 268, 114, 22);
		frame.getContentPane().add(food_descript);
		
		JButton btnNewButton = new JButton("Home");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.dispose();
				Homepage main = new Homepage();
				main.frame.setVisible(true);
			}
		});
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnNewButton.setBounds(344, 414, 130, 25);
		frame.getContentPane().add(btnNewButton);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(344, 57, 432, 347);
		frame.getContentPane().add(scrollPane);
		
		table = new JTable();
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				int i = table.getSelectedRow();
				food_id.setText(model.getValueAt(i, 0).toString());
				food_name.setText(model.getValueAt(i, 1).toString());
				food_price.setText(model.getValueAt(i, 2).toString());
				food_descript.setText(model.getValueAt(i, 3).toString());
			}
		});
		table.setBackground(SystemColor.activeCaptionBorder);
		model = new DefaultTableModel();
		Object[] column = {"ID","Food Name","Price","Discription" };
		Object[] row = new Object[4];
		model.setColumnIdentifiers(column);
		table.setModel(model);
		
		scrollPane.setViewportView(table);
		
		JButton btnNewButton_2 = new JButton("Add Food");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				if(food_id.getText().equals("") || food_name.getText().equals("") || food_price.getText().equals("") || food_descript.getText().equals("")  ){
					JOptionPane.showMessageDialog(null, "Please fill complete informations");
				}
				else {
					row[0] = food_id.getText();
					row[1] = food_name.getText();
					row[2] = food_price.getText();
					row[3] = food_descript.getText();
					
					
					food_id.setText("");
					food_name.setText("");
					food_price.setText("");
					food_descript.setText("");
					JOptionPane.showMessageDialog(null, " Food item is added succesfully ");
				}
				
				
				
				
				
				model.addRow(row);
			}
		});
		btnNewButton_2.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnNewButton_2.setBounds(24, 340, 130, 21);
		frame.getContentPane().add(btnNewButton_2);
		
		JButton btnNewButton_3 = new JButton("Delete Food");
		btnNewButton_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int i = table.getSelectedRow();
				if(i>=0) {
					model.removeRow(i);
					JOptionPane.showMessageDialog(null, " Food item is removed ! ");
					
				}
				else {
					JOptionPane.showMessageDialog(null, " Please select row first !");
				}
			}
		});
		btnNewButton_3.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnNewButton_3.setBounds(24, 383, 130, 21);
		frame.getContentPane().add(btnNewButton_3);
		
		JButton btnNewButton_1 = new JButton("Clear  Fields");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				food_id.setText("");
				food_name.setText("");
				food_price.setText("");
				food_descript.setText("");
				
			}
		});
		btnNewButton_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnNewButton_1.setBounds(187, 383, 114, 21);
		frame.getContentPane().add(btnNewButton_1);
		
		JButton btnNewButton_4 = new JButton("Update Food");
		btnNewButton_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int i = table.getSelectedRow();
				if(i>=0) {
					model.setValueAt(food_id.getText(), i, 0);
					model.setValueAt(food_name.getText(), i, 1);
					model.setValueAt(food_price.getText(), i, 2);
					model.setValueAt(food_descript.getText(), i, 3);
					JOptionPane.showMessageDialog(null, " Update Successfully");
				}
				else {
					JOptionPane.showMessageDialog(null, " Please select row first !");
				}
				
				
			}
		});
		btnNewButton_4.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnNewButton_4.setBounds(187, 340, 114, 21);
		frame.getContentPane().add(btnNewButton_4);
		
		
		JButton btnProceed = new JButton("Proceed to database");
		btnProceed.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnProceed.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				
if (ae.getSource()== btnProceed) {
					
				    try {  
				        Class.forName("com.mysql.jdbc.Driver");  
				        // establish connection  
				        java.sql.Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/food_menu", "root", "");   
				        java.sql.Statement statement = conn.createStatement();
				        
				        
				       statement.executeUpdate("INSERT INTO `food_table` (`food_id`,`food_name`,`food_price`,`food_descript`) " + "VALUES ('" + food_id.getText() + "','" + food_name.getText()  +"','"+ food_price.getText() + "','" + food_descript.getText() +"')"); 
				       JOptionPane.showMessageDialog(null, "Food added to the database CHECK !");
				        
				       statement.close(); 
				       conn.close();  
				     
				    } catch (SQLException | ClassNotFoundException e) {  
				        JOptionPane.showMessageDialog(null, e);  
				    }  
				}


if(food_id.getText().equals("") || food_name.getText().equals("") || food_price.getText().equals("") || food_descript.getText().equals("")  ){
	JOptionPane.showMessageDialog(null, "Please fill complete informations");
}
else {
	row[0] = food_id.getText();
	row[1] = food_name.getText();
	row[2] = food_price.getText();
	row[3] = food_descript.getText();
	
	
	food_id.setText("");
	food_name.setText("");
	food_price.setText("");
	food_descript.setText("");
	JOptionPane.showMessageDialog(null, " Food item is added succesfully ");
}





model.addRow(row);
				
				
			}
		});
		btnProceed.setBounds(484, 415, 143, 24);
		frame.getContentPane().add(btnProceed);
		
		JButton btnNewButton_5 = new JButton("Clear database");
		btnNewButton_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				
if (ae.getSource()== btnNewButton_5) {
					
				    try {  
				        Class.forName("com.mysql.jdbc.Driver");  
				        // establish connection  
				        java.sql.Connection con = DriverManager.getConnection("jdbc:mysql://localhost/food_menu", "root", "");   
				        java.sql.Statement statement2 = con.createStatement();
				        
				        
				       statement2.executeUpdate("DELETE  FROM food_table"); 
				       JOptionPane.showMessageDialog(null, "ALL FOOD ITEMS CLEARED  ! ");
				        
				       statement2.close(); 
				       con.close();  
				     
				    } catch (SQLException | ClassNotFoundException e1) {  
				        JOptionPane.showMessageDialog(null, e1);  
				    }  
				}
				
				
			}
		});
		btnNewButton_5.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnNewButton_5.setBounds(637, 414, 139, 24);
		frame.getContentPane().add(btnNewButton_5);
		
		JLabel imgLabel = new JLabel("");
		imgLabel.setIcon(new ImageIcon("C:\\Users\\User\\Desktop\\ad.jpg"));
		imgLabel.setBounds(-13, 10, 872, 492);
		frame.getContentPane().add(imgLabel);
		
}
	}

