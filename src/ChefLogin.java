import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.JPasswordField;


public class ChefLogin {

	JFrame frmChefLogin;
	private JTextField chefUserName;
	private JPasswordField chefPwd;

	/**
	 * Launch the application.
	 */
	public static void chef() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ChefLogin window = new ChefLogin();
					window.frmChefLogin.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public ChefLogin() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmChefLogin = new JFrame();
		frmChefLogin.setTitle("CHEF LOGIN");
		frmChefLogin.setBounds(100, 100, 450, 500);
		frmChefLogin.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		frmChefLogin.addWindowListener(new Exit());
		frmChefLogin.getContentPane().setLayout(null);
		
		try {
			frmChefLogin.setIconImage(ImageIO.read(getClass().getResourceAsStream("/Images/icon.PNG")));
        } catch (IOException e) {
            e.printStackTrace();
        }
		
		JPanel panel = new JPanel() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			public void paintComponent(Graphics g) {  
        		Image img = Toolkit.getDefaultToolkit().getImage(  
        				AdminLogin.class.getResource("/Images/chef1.jpg"));  
        		g.drawImage(img, 0, 0, this.getWidth(), this.getHeight(), this);  
        	} 
		};
		panel.setBounds(0, 0, 450, 478);
		frmChefLogin.getContentPane().add(panel);
		panel.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("CHEF LOGIN");
		lblNewLabel.setForeground(new Color(178, 34, 34));
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setFont(new Font("Dialog", Font.BOLD, 36));
		lblNewLabel.setBounds(10, 11, 414, 75);
		panel.add(lblNewLabel);
		
		JLabel usernameChef = new JLabel("USER NAME");
		usernameChef.setFont(new Font("Dialog", Font.BOLD, 20));
		usernameChef.setBounds(26, 214, 144, 35);
		panel.add(usernameChef);
		
		JLabel passwordChef = new JLabel("PASSWORD");
		passwordChef.setFont(new Font("Dialog", Font.BOLD, 20));
		passwordChef.setBounds(26, 273, 115, 35);
		panel.add(passwordChef);
		
		chefUserName = new JTextField();
		chefUserName.setFont(new Font("Dialog", Font.BOLD, 22));
		chefUserName.setOpaque(false);
		chefUserName.setBounds(199, 215, 204, 41);
		panel.add(chefUserName);
		chefUserName.setColumns(10);
		
		JButton chefLogin = new JButton("LOGIN");
		chefLogin.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent e) {
				if (chefUserName.getText().equals("Chef") && chefPwd.getText().equals("12345")) {
					frmChefLogin.dispose();
                    Cheff_UI pageA = new Cheff_UI();
                    pageA.frame.setVisible(true);
    				JOptionPane.showMessageDialog(null, "Login Successfully..");
				}
				else {
					JOptionPane.showMessageDialog(null, "Enter Valid Username and Password");
					chefUserName.setText(" ");
					chefPwd.setText(" ");
				}
			}
		});
		chefLogin.setForeground(new Color(178, 34, 34));
		chefLogin.setFont(new Font("Dialog", Font.BOLD, 20));
		chefLogin.setBounds(141, 346, 144, 39);
		panel.add(chefLogin);
		
		JCheckBox chckbxNewCheckBox = new JCheckBox("Show Password");
		chckbxNewCheckBox.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		if (chckbxNewCheckBox.isSelected()) {
        			chefPwd.setEchoChar((char) 0);
                } else {
                	chefPwd.setEchoChar('*');
                }
        	}
        });
		chckbxNewCheckBox.setOpaque(false);
		chckbxNewCheckBox.setBounds(199, 316, 144, 23);
		panel.add(chckbxNewCheckBox);
		
		JButton btnNewButton = new JButton("Admin Login");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frmChefLogin.dispose();
				AdminLogin admin = new AdminLogin();
				admin.frmAdminLogin.setVisible(true);
			}
		});
		btnNewButton.setForeground(new Color(0, 0, 139));
		btnNewButton.setFont(new Font("Dialog", Font.PLAIN, 14));
		btnNewButton.setBounds(288, 411, 115, 23);
		panel.add(btnNewButton);
		
		JButton btnMainPage = new JButton("Main Page");
		btnMainPage.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frmChefLogin.dispose();
				Homepage main = new Homepage();
				main.frame.setVisible(true);
			}
		});
		btnMainPage.setForeground(new Color(0, 0, 139));
		btnMainPage.setFont(new Font("Dialog", Font.PLAIN, 14));
		btnMainPage.setBounds(37, 413, 115, 23);
		panel.add(btnMainPage);
		
		chefPwd = new JPasswordField();
		chefPwd.setFont(new Font("Dialog", Font.BOLD, 22));
		chefPwd.setEchoChar('*');
		chefPwd.setBounds(199, 261, 204, 41);
		panel.add(chefPwd);
	}
	private class Exit extends WindowAdapter {
		public void windowClosing(WindowEvent e) {
			int confirmed = JOptionPane.showConfirmDialog(null,"Do You Want To EXIT?","EXIT",JOptionPane.YES_NO_OPTION);
			if(confirmed == JOptionPane.YES_OPTION){
				frmChefLogin.dispose();
		    }else{
		    	frmChefLogin.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
			}
		}
	}
	
	
}


