import java.awt.EventQueue;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;

import javax.swing.SwingConstants;



import java.awt.Color;
import javax.swing.JTextField;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JPasswordField;

public class AdminLogin {

	JFrame frmAdminLogin;
	private JTextField adminUserName;
	private JPasswordField adminPwd;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AdminLogin window = new AdminLogin();
					window.frmAdminLogin.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public AdminLogin() {
		
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		
		
		frmAdminLogin = new JFrame();
		frmAdminLogin.setTitle("ADMIN LOGIN");
		frmAdminLogin.setBounds(100, 100, 450, 500);
		frmAdminLogin.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		frmAdminLogin.addWindowListener(new Exit());
		frmAdminLogin.getContentPane().setLayout(null);
		
		JPanel panel = new JPanel() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			public void paintComponent(Graphics g) {  
        		Image img = Toolkit.getDefaultToolkit().getImage(  
        				AdminLogin.class.getResource("/Images/wallpaper.jpg"));  
        		g.drawImage(img, 0, 0, this.getWidth(), this.getHeight(), this);  
        	} 
		};
		
		
		panel.setBackground(new Color(255, 250, 205));
		panel.setBounds(0, 0, 434, 461);
		frmAdminLogin.getContentPane().add(panel);
		panel.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("ADMIN LOGIN");
		lblNewLabel.setForeground(new Color(178, 34, 34));
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setFont(new Font("Dialog", Font.BOLD, 36));
		lblNewLabel.setBounds(10, 11, 414, 75);
		panel.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("USER NAME");
		lblNewLabel_1.setFont(new Font("Dialog", Font.BOLD, 20));
		lblNewLabel_1.setBounds(26, 229, 144, 35);
		panel.add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("PASSWORD");
		lblNewLabel_2.setFont(new Font("Dialog", Font.BOLD, 20));
		lblNewLabel_2.setBounds(26, 288, 115, 35);
		panel.add(lblNewLabel_2);
		
		adminUserName = new JTextField();
		adminUserName.setFont(new Font("Dialog", Font.BOLD, 22));
		adminUserName.setOpaque(false);
		adminUserName.setBounds(199, 230, 204, 41);
		panel.add(adminUserName);
		adminUserName.setColumns(10);
		
		JButton adminLogin = new JButton("LOGIN");
		adminLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (adminUserName.getText().equals("Admin") && adminPwd.getText().equals("12345")) {
					frmAdminLogin.dispose();
                    AdminPage pageA = new AdminPage();
                    pageA.frame.setVisible(true);
    				JOptionPane.showMessageDialog(null, "Login Successfully..");
				}
				else {
					JOptionPane.showMessageDialog(null, "Enter Valid Username and Password");
					adminUserName.setText(" ");
					adminPwd.setText(" ");
				}
			}
		});
		adminLogin.setForeground(new Color(178, 34, 34));
		adminLogin.setFont(new Font("Dialog", Font.BOLD, 20));
		adminLogin.setBounds(146, 361, 144, 39);
		panel.add(adminLogin);

		JCheckBox chckbxNewCheckBox = new JCheckBox("Show Password");
		chckbxNewCheckBox.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		if (chckbxNewCheckBox.isSelected()) {
        			adminPwd.setEchoChar((char) 0);
                } else {
                	adminPwd.setEchoChar('*');
                }
        	}
        });
		chckbxNewCheckBox.setOpaque(false);
		chckbxNewCheckBox.setBounds(199, 331, 124, 23);
		panel.add(chckbxNewCheckBox);
		
		JButton btnNewButton = new JButton("Chef Login");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frmAdminLogin.dispose();
				ChefLogin admin = new ChefLogin();
				admin.frmChefLogin.setVisible(true);
			}
		});
		btnNewButton.setForeground(new Color(0, 0, 139));
		btnNewButton.setFont(new Font("Dialog", Font.PLAIN, 14));
		btnNewButton.setBounds(288, 411, 115, 23);
		panel.add(btnNewButton);
		
		JLabel lblNewLabel_3 = new JLabel("");
		
		lblNewLabel_3.setHorizontalTextPosition(SwingConstants.CENTER);
        Image img = new ImageIcon(this.getClass().getResource("/Images/admin.jpg")).getImage();
        lblNewLabel_3.setIcon(new ImageIcon(img));
		
		lblNewLabel_3.setBounds(80, 84, 263, 134);
		panel.add(lblNewLabel_3);
		
		JButton btnMainPage = new JButton("Main Page");
		btnMainPage.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frmAdminLogin.dispose();
				Main main = new Main();
				main.frame.setVisible(true);
			}
		});
		btnMainPage.setForeground(new Color(0, 0, 139));
		btnMainPage.setFont(new Font("Dialog", Font.PLAIN, 14));
		btnMainPage.setBounds(26, 411, 115, 23);
		panel.add(btnMainPage);
		
		adminPwd = new JPasswordField();
		adminPwd.setFont(new Font("Dialog", Font.BOLD, 22));
		adminPwd.setEchoChar('*');
		adminPwd.setOpaque(false);
		adminPwd.setBounds(199, 275, 204, 41);
		panel.add(adminPwd);
		
		
	}
	private class Exit extends WindowAdapter {
		public void windowClosing(WindowEvent e) {
			int confirmed = JOptionPane.showConfirmDialog(null,"Do You Want To EXIT?","EXIT",JOptionPane.YES_NO_OPTION);
			if(confirmed == JOptionPane.YES_OPTION){
				frmAdminLogin.dispose();
		    }else{
				frmAdminLogin.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
			}
		}
	}
}
